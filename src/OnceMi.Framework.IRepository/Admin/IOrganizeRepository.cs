﻿using FreeSql;
using OnceMi.Framework.Entity.Admin;

namespace OnceMi.Framework.IRepository
{
    public interface IOrganizeRepository : IBaseRepository<Organizes, long>, IRepositoryDependency
    {

    }
}
