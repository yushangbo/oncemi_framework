﻿using FreeSql;
using OnceMi.Framework.Entity.Admin;

namespace OnceMi.Framework.IRepository
{
    public interface IViewRepository : IBaseRepository<Views, long>, IRepositoryDependency
    {

    }
}
