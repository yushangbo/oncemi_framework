﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnceMi.Framework.Model.Dto
{
    public class OrganizeTypeResponse
    {
        public string Name { get; set; }

        public string Value { get; set; }
    }
}
