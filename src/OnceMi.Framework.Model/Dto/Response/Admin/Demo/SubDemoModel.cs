﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnceMi.Framework.Model.Dto
{
    public class SubDemoModel
    {
        public string Title { get; set; }

        public DateTime Time { get; set; }

        public int Span { get; set; }
    }
}
