﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnceMi.Framework.Model.Enums
{
    public static class ApiVersions
    {
        public const string V1 = "1";

        public const string V2 = "2";
    }
}
