﻿using FreeSql;
using OnceMi.Framework.Entity.Admin;

namespace OnceMi.Framework.IRepository
{
    public interface IMenuRepository : IBaseRepository<Menus, long>, IRepositoryDependency
    {

    }
}
