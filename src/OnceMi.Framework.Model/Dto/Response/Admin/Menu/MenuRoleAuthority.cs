﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnceMi.Framework.Model.Dto.Response.Admin.Menu
{
    public class MenuRoleAuthority
    {
        public long Role { get; set; }
    }
}
