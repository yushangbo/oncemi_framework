﻿using FreeSql;
using OnceMi.Framework.Entity.Admin;

namespace OnceMi.Framework.IRepository
{
    public interface IJobGroupRepository : IBaseRepository<JobGroups, long>, IRepositoryDependency
    {

    }
}
