﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnceMi.Framework.Model.Dto.Request.Admin.User
{
    public class UserAvatarUrlParamModel
    {
        public string Key { get; set; }
    }
}
