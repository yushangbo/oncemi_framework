﻿using FreeSql;
using OnceMi.Framework.Entity.Admin;

namespace OnceMi.Framework.IRepository
{
    public interface IRoleRepository : IBaseRepository<Roles, long>, IRepositoryDependency
    {

    }
}
