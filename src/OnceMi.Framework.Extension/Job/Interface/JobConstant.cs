﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnceMi.Framework.Extension.Job
{
    public class JobConstant
    {
        public const string Id = "Id";

        public const string Name = "Name";

        public const string EndTime = "EndTime";

        public const string IsTrigger = "IsTrigger";
    }
}
